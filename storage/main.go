package storage

import (
	"bitbucket.org/Udevs/position_service/storage/postgres"
	"bitbucket.org/Udevs/position_service/storage/repo"
	"github.com/jmoiron/sqlx"
)

type StorageI interface {
	Attribute() repo.AttributeRepoI
	Position() repo.PositionRepoI
	Profession() repo.ProfessionRepoI
}

type storagePG struct {
	attribute  repo.AttributeRepoI
	position   repo.PositionRepoI
	profession repo.ProfessionRepoI
}

func NewStoragePG(db *sqlx.DB) StorageI {
	return &storagePG{
		attribute:  postgres.NewAttributeRepo(db),
		position:   postgres.NewPositionRepo(db),
		profession: postgres.NewProfessionRepo(db),
	}
}

func (s *storagePG) Attribute() repo.AttributeRepoI {
	return s.attribute
}

func (s *storagePG) Position() repo.PositionRepoI {
	return s.position
}

func (s *storagePG) Profession() repo.ProfessionRepoI {
	return s.profession
}
