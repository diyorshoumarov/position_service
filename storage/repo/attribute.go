package repo

import "bitbucket.org/Udevs/position_service/genproto/position_service"

type AttributeRepoI interface {
	Create(req *position_service.CreateAttribute) (string, error)
	Get(id string) (*position_service.Attribute, error)
	GetAll(req *position_service.GetAllAttributeRequest) (*position_service.GetAllAttributeResponse, error)
	Update(req *position_service.Attribute) (*position_service.MsgResponse, error)
	Delete(id string) (*position_service.MsgResponse, error)
}
