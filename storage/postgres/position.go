package postgres

import (
	"bitbucket.org/Udevs/position_service/genproto/position_service"
	"bitbucket.org/Udevs/position_service/storage/repo"
	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
)

type positionRepo struct {
	db *sqlx.DB
}

func NewPositionRepo(db *sqlx.DB) repo.PositionRepoI {
	return &positionRepo{
		db: db,
	}
}

func (r *positionRepo) Create(req *position_service.CreatePosition) (string, error) {
	var id uuid.UUID

	tx, err := r.db.Begin()

	if err != nil {
		return "", err
	}

	defer func() {
		if err != nil {
			tx.Rollback()
		} else {
			tx.Commit()
		}
	}()

	id, err = uuid.NewRandom()
	if err != nil {
		return "", err
	}

	queryPos := `
		INSERT INTO position
			(
				id,
				name,
				profession_id,
				company_id
			)
		VALUES
			(
				$1,
				$2,
				$3,
				$4
			)
	`

	_, err = tx.Exec(queryPos,
		id,
		req.Name,
		req.ProfessionId,
		req.CompanyId,
	)

	if err != nil {
		return "", err
	}

	queryPosAtt := `
	INSERT INTO position_attributes
		(
			id,
			attribute_id,
			position_id,
			value
		)
	VALUES
		(
			$1,
			$2,
			$3,
			$4
		)
	`

	for _, val := range req.PositionAttributes {
		var idAtt uuid.UUID

		idAtt, err = uuid.NewRandom()
		if err != nil {
			return "", err
		}

		_, err = tx.Exec(queryPosAtt,
			idAtt,
			val.AttributeId,
			id,
			val.Value,
		)

		if err != nil {
			return "", err
		}
	}

	return id.String(), nil
}

func (r *positionRepo) Get(id string) (*position_service.Position, error) {
	var position position_service.Position

	queryPos := `
		SELECT id, name, profession_id, company_id
		FROM position
		WHERE id = $1
	`

	row := r.db.QueryRow(queryPos, id)
	err := row.Scan(
		&position.Id,
		&position.Name,
		&position.ProfesssionId,
		&position.CompanyId,
	)

	if err != nil {
		return nil, err
	}

	queryPosAtt := `
			SELECT id, attribute_id, position_id, value
			FROM position_attributes
			WHERE position_id = $1
		`

	rows, err := r.db.Query(queryPosAtt, id)

	if err != nil {
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		var position_attribute position_service.GetPositionAttribute

		err = rows.Scan(
			&position_attribute.Id,
			&position_attribute.AttributeId,
			&position_attribute.PositionId,
			&position_attribute.Value,
		)

		if err != nil {
			return nil, err
		}

		var attribute position_service.Attribute

		queryAtt := `
			SELECT id, name, type
			FROM attribute
			WHERE id = $1
		`

		row := r.db.QueryRow(queryAtt, position_attribute.AttributeId)
		err = row.Scan(
			&attribute.Id,
			&attribute.Name,
			&attribute.Type,
		)

		if err != nil {
			return nil, err
		}

		position_attribute.Attribute = &attribute

		position.PositionAttributes = append(position.PositionAttributes, &position_attribute)
	}

	return &position, nil
}

func (r *positionRepo) GetAll(req *position_service.GetAllPositionRequest) (*position_service.GetAllPositionResponse, error) {
	var args = make(map[string]interface{})
	var positions []*position_service.Position
	var filter string
	var count uint32

	if req.Name != "" {
		filter += ` AND name ILIKE '%' || :name || '%' `
		args["name"] = req.Name
	}

	if req.CompanyId != "" {
		filter += ` AND company_id = :company_id `
		args["company_id"] = req.CompanyId
	}

	if req.ProfessionId != "" {
		filter += ` AND profession_id = :profession_id `
		args["profession_id"] = req.ProfessionId
	}

	countQuery := `
		SELECT count(1)
		FROM position
		WHERE true
		` + filter

	rows, err := r.db.NamedQuery(countQuery, args)

	if err != nil {
		return nil, err
	}

	for rows.Next() {
		err = rows.Scan(
			&count,
		)

		if err != nil {
			return nil, err
		}
	}

	filter += `OFFSET :offset LIMIT :limit`
	args["limit"] = req.Limit
	args["offset"] = req.Offset

	queryPos := `
			SELECT id, name, profession_id, company_id
			FROM position
			WHERE true
		` + filter

	rows, err = r.db.NamedQuery(queryPos, args)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var position position_service.Position

		err := rows.Scan(
			&position.Id,
			&position.Name,
			&position.ProfesssionId,
			&position.CompanyId,
		)

		if err != nil {
			return nil, err
		}

		queryPosAtt := `
					SELECT id, attribute_id, position_id, value
					FROM position_attributes
					WHERE position_id = $1
				`

		rows, err := r.db.Query(queryPosAtt, position.Id)

		if err != nil {
			return nil, err
		}

		defer rows.Close()

		for rows.Next() {
			var position_attribute position_service.GetPositionAttribute

			err = rows.Scan(
				&position_attribute.Id,
				&position_attribute.AttributeId,
				&position_attribute.PositionId,
				&position_attribute.Value,
			)

			if err != nil {
				return nil, err
			}

			var attribute position_service.Attribute

			queryAtt := `
					SELECT id, name, type
					FROM attribute
					WHERE id = $1
				`

			row := r.db.QueryRow(queryAtt, position_attribute.AttributeId)
			err = row.Scan(
				&attribute.Id,
				&attribute.Name,
				&attribute.Type,
			)

			if err != nil {
				return nil, err
			}

			position_attribute.Attribute = &attribute

			position.PositionAttributes = append(position.PositionAttributes, &position_attribute)
		}

		positions = append(positions, &position)
	}

	return &position_service.GetAllPositionResponse{
		Positions: positions,
		Count:     count,
	}, nil
}

func (r *positionRepo) Update(req *position_service.UpdatePosition) (*position_service.MsgResponse, error) {
	tx, err := r.db.Begin()

	if err != nil {
		return &position_service.MsgResponse{
			Msg: "",
		}, err
	}

	defer func() {
		if err != nil {
			tx.Rollback()
		} else {
			tx.Commit()
		}
	}()

	queryPos := `
		UPDATE position
		SET 
			name = $2, 
			profession_id = $3, 
			company_id = $4
		WHERE id = $1
	`

	_, err = tx.Exec(queryPos, req.Id, req.Name, req.ProfessionId, req.CompanyId)
	if err != nil {
		return &position_service.MsgResponse{
			Msg: "",
		}, err
	}

	queryPosAtt := `
		UPDATE position_attributes
		SET value = $3
		WHERE position_id = $1
		AND attribute_id = $2
	`
	for _, val := range req.PositionAttributes {
		_, err = tx.Exec(queryPosAtt, req.Id, val.AttributeId, val.Value)
		if err != nil {
			return &position_service.MsgResponse{
				Msg: "",
			}, err
		}
	}

	return &position_service.MsgResponse{
		Msg: "Updated",
	}, nil

}

func (r *positionRepo) Delete(id string) (*position_service.MsgResponse, error) {

	tx, err := r.db.Begin()

	if err != nil {
		return &position_service.MsgResponse{
			Msg: "",
		}, err
	}

	defer func() {
		if err != nil {
			tx.Rollback()
		} else {
			tx.Commit()
		}
	}()

	queryPosAtt := `
		DELETE FROM position_attributes
		WHERE position_id = $1
	`

	_, err = tx.Exec(queryPosAtt, id)

	if err != nil {
		return &position_service.MsgResponse{
			Msg: "Not Deleted",
		}, err
	}

	queryPos := `
		DELETE FROM position
		WHERE id = $1
	`

	_, err = tx.Exec(queryPos, id)

	if err != nil {
		return &position_service.MsgResponse{
			Msg: "Not Deleted",
		}, err
	}

	return &position_service.MsgResponse{
		Msg: "Deleted",
	}, err
}
