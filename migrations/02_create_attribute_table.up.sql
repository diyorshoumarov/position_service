BEGIN;

CREATE TYPE attribute_type AS enum('text', 'number', 'datetime');

CREATE TABLE IF NOT EXISTS attribute (
    id uuid PRIMARY KEY,
    name varchar(255) NOT NULL,
    type attribute_type NOT NULL
);

COMMIT;